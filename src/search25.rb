require 'rubygems'
require 'sinatra'
require "sinatra/config_file"
require 'pg'
require 'builder'
require 'xmlsimple'
require 'json'
require 'yaml'

use Rack::CommonLogger # fix for bug in Sinatra 1.3.2

# Global paths to configuration files

# get the absolute path to this script
$PATH = File.expand_path(File.dirname(__FILE__))

# config file is kept outside the git-managed directory
config_file $PATH + '/../../search25.yml'

# path to instruction list
$DATAPATH = $PATH + '/data/commands.txt'

# make the database schema globally available
schema = File.open($PATH+'/data/schema.json').read
$SCHEMA = JSON.parse(schema)

$DBCONN = nil

=begin rdoc
Sinatra-based version of the Search25 API for the M25 Library Consortium
library subject and access database. Intended to be used both with the
Xerxes Search25 tool, and as a free-standing public API.

API calls are REST style, specifying resources and sub-resources. Output
is json or XML, as requested by file suffix (default in XML). Access 
information depends on user status; user status is passed as an eduPerson
style affiliation, consisting of a role and domain in email-style format.

GET calls are public; access to PUT, POST and DELETE is limited to specified
hosts by configuration file.

More documentation can be found on the Search25 wiki.

To execute:

ruby search25.rb -e [production|test|development] nohup &

Author Graham Seaman (graham.seaman@libretech.co.uk) 2012-06-01
Copyright M25 Consortium of Academic Libraries
License MIT license as for Sinatra itself. See LICENSE file

=end

# Configuration options
configure do

  class Db_connector
  	def initialize(host, port, name, user, pass)  
   		@host = host
		@port = port  
    	@name = name
		@user = user
		@pass = pass
	end

	def connect
		@conn = PG::connect(@host, @port, '', '', @name, @user, @pass)
  	end

	def getConn
		#status = @conn.connect_poll
		##if (status != PG::CONNECTION_OK)
		#if (status != PG::PGRES_POLLING_OK)
		#	@conn.finish
		#	connect	
		#end
		#return @conn
		connect
		return @conn
	end

  end  
      
  $DBCONN = Db_connector.new(settings.dbhost, settings.dbport, settings.dbname, settings.dbuser, settings.dbpass)

  $DBCONN.connect

#  conn = PG::connect(settings.dbhost, settings.dbport, '', '', settings.dbname, settings.dbuser, settings.dbpass)
#  set :pg, conn
#  set :raise_errors, false 
  set :logging, settings.logging
  set :port, settings.port

  log_file = File.new(settings.logfile, "a")
  $stdout.reopen(log_file)
  $stderr.reopen(log_file)


	
end

# Non-existent routes return a list of valid routes with explanation
not_found do
  content_type 'application/xml'
  builder do |xml|
    xml.instruct!
    xml.s25(:error, :'xmlns:s25' => 'http://search25.ac.uk/ns/0.1', :cmd => request.path) do
      xml.s25(:message, 'No such command. Please select a command from the list') 
      xml.tag!('s25:commands') do
        File.open($DATAPATH).each do |line| 
          (cmd, desc) = line.split(' - ')
          xml.s25(:command, desc, :cmd => cmd) 
        end
      end
    end
  end
end

# Routes may throw a NoResultError exception if a database query
# returns nothing.
class NoResultError < Exception; end

error NoResultError do
    env['sinatra.error'].message
  content_type 'application/xml'
  builder do |xml|
    xml.instruct!
    xml.s25(:error, :'xmlns:s25' => 'http://search25.ac.uk/ns/0.1', :cmd => request.path) do
      xml.s25(:message, 'No results found. Please check the parameters used') 
    end
  end
end

# Routes may throw an InvalidParameterError exception if a parameter is malformed
class InvalidParameterError < Exception; end

error InvalidParameterError do
    env['sinatra.error'].message
  content_type 'application/xml'
  builder do |xml|
    xml.instruct!
    xml.s25(:error, :'xmlns:s25' => 'http://search25.ac.uk/ns/0.1', :cmd => request.path) do
      xml.s25(:message, 'Invalid parameter used') 
    end
  end
end

# refuse access for database writes unless client is on authorised host
before do
  method = request.request_method.downcase
  if ( method == 'put' || method == 'post' || method == 'delete' )
    if request.ip != settings.allowedclient
      halt 401
    end
  end
end

helpers do

  # set up http basicauth for login (used by function 'protected')
  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? && @auth.basic? && @auth.credentials && @auth.credentials == [settings.basicuser, settings.basicpass]
  end

  # basicauth protection for individual functions
  # Used for admin function only: put, post, delete protected by 'before' 
  # add 'protected!' as first line in function to use
  def protected!
    unless authorized?
      response['WWW-Authenticate'] = %(Basic realm="Restricted Area")
      throw(:halt, [401, "Not authorized\n"])
    end
  end

  # convert http params to pg format ready for database write
  def pg_params( params, schematable )

    # remove sinatra-internal params
    params.delete('splat')  # see https://groups.google.com/forum/?fromgroups=#!topic/sinatrarb/CvS0DmENTa8
    params.delete('captures')

    numbers = ['integer', 'float']
    fields = $SCHEMA[schematable]['fields'] # get the field descriptions

    deletes = Array.new

    params.each { |key, value|
      # check whether an unused or a non-null empty value
      if !fields[key] || (fields[key]['sense'] == 'nonnull' && value == '')
        deletes.push(key)  # can't delete in place or upset loop
      else
        # get type for each of the fields from $SCHEMA
        ftype = fields[key]['type']
        if ftype == 'boolean'
          case value
            when 'on' 
              params[key] = 'true'
            when 'off' 
              params[key] = 'false'
          end
        elsif numbers.include? ftype # some kind of number
          if value.empty?
            value = 'null'
          end
          params[key] = value
        else # some kind of string
          # add sql quotes if required
          #params[key] = "'#{settings.pg.escape_string(value)}'" 
          params[key] = "'#{$DBCONN.getConn.escape_string(value)}'" 
        end
      end
    }

    deletes.each{ |key|
      params.delete(key)
    }

  end

  def database_create( params, table, idname, schematable, location, allowed )

    pg_params( params, schematable )

    sql = "INSERT INTO #{table} (" + params.keys.join(',') + ") VALUES (" + params.values.join(',') + ") RETURNING #{idname} AS id"

    #res = settings.pg.exec(sql)
    res = $DBCONN.getConn.exec(sql)
    rows = res.cmd_tuples # return no of rows affected (0 if error)

    if rows == 0
      halt 500
    else
      location += res[0]['id']
	# for some reason halt 201 won't take an array parameter in this version - it just expects the url
        # halt 201, {'Location' => location, 'Allowed' => allowed.join(',') }
      halt 201, location
    end

  end

  def database_update( params, table, schematable, where_sql, location, allowed )

    pg_params( params, schematable )

    # may be a new item or an edit: check which
    sql = "SELECT COUNT(*) FROM #{table} " + where_sql 
    #res = settings.pg.exec(sql)
    res = $DBCONN.getConn.exec(sql)
    already_exists = res.getvalue(0,0).to_i
    if already_exists > 0 # it's an edit of an existing item
      sql = "UPDATE #{table} SET "
      parr = Array.new
      params.each { |key, value|
        parr.push( "#{key} = #{value}" )
      }
      plist = parr.join(',')
      sql += plist
      sql += where_sql
    else # its a new item
      sql = "INSERT INTO #{table} (" + params.keys.join(',') + ") VALUES (" + params.values.join(',') + ")"
    end

    #res = settings.pg.exec(sql)
    res = $DBCONN.getConn.exec(sql)
    rows = res.cmd_tuples # return no of rows affected (0 if error)

    if rows == 0
      halt 500
    else
      if already_exists
        halt 204, {'Location' => location, 'Allowed' => allowed.join(',') }
      else
        halt 201, {'Location' => location, 'Allowed' => allowed.join(',') }
      end
    end

  end

  def database_delete( params, table, schematable, keynames )
    
    pg_params( params, schematable )

    keys = Array.new
    keynames.each { | keyname |
      val = params[keyname.to_sym] 
      keys.push( "#{keyname}=#{val}" )
    }
    sql = "DELETE FROM #{table} WHERE " + keys.join(' AND ')
    #res = settings.pg.exec(sql)
    res = $DBCONN.getConn.exec(sql)
    rows = res.cmd_tuples # return no of rows affected (0 if error)

    if rows != 1
      halt 500, sql
    else 
      halt 204
    end

  end

  # Allow user to choose between institutional id sources
  # Note that only the m25 id covers all M25 members
  def select_institutional_code(code,codeval)
    if (code =~ /^m25|copac|isil|sconul$/)
      " i.#{params[:code]}_code='#{codeval}'" 
    else # default to m25_code
      " i.m25_code='#{codeval}' "
    end
  end

  # Convert a Postgres Result to a hasharray for JSON
  def result_to_hasharray(res)
    arr = Array.new
    res.each do |row|
      arr.push row
    end
    arr
  end

  # Generic routine to convert Postgres Result to XML, using database
  # fieldnames within s25 namespace.
  # result is the Postgres Result
  # resource is the root name to use; resource is its plural
  # id is the name of the primary key
  def result_to_xml(result, resource, resources, id)
      builder do |xml|
        xml.instruct!
        xml.s25(:request, :'xmlns:s25' => 'http://search25.ac.uk/ns/0.1', :url => request.url) do
          xml.tag! 's25:'+resources do
            result.each do |row|
              xml.tag!('s25:'+resource, :id => row[id], :id_name => id)  do
                row.each do |key, val|
                    xml.tag!('s25:'+key, val) unless val.nil? || val.empty?  
                end
              end
            end
          end
        end
      end
  end

  # return an empty form for a new table entry based on schema.json contents
  def empty_form(table, format, code=nil)

    # make deep copy of field descriptions so we can write to them safely
    form = Marshal.load( Marshal.dump($SCHEMA[table]) )

    # if we have a parent id for the item, insert it
    if ! code.nil?
      form['fields'][code.keys[0]]['value'] = code.values[0]
    end

    case format
      when 'json'
        {table => form}.to_json
      else # default is xml 
        XmlSimple.xml_out( form, {'RootName' => table, 'XmlDeclaration' => true} ) 
    end 

  end

  # return a form populated with table data to edit a table entry
  def populated_form(table, sql, format)

    # make deep copy of field descriptions so we can write to them safely
    form = Marshal.load( Marshal.dump($SCHEMA[table]) )
  
    # fetch the values from the db
    #res  = settings.pg.exec(sql)
    res = $DBCONN.getConn.exec(sql)

    if res.ntuples == 0 then
      raise NoResultError, 'no results'
    end  

    res = result_to_hasharray(res)
    res = res[0]

    # populate the form with the values
    res.each_pair do |k,v|
      if form['fields'].has_key?(k)  # some db fields will be populated automatically
        form['fields'][k]['value'] = v || '' # simplexml dies on nil values
      end
    end

    case format
      when 'json'
        {table => form}.to_json
      else # default is xml 
        XmlSimple.xml_out(form, {'RootName' => table, 'XmlDeclaration' => true}) 
    end 

  end

end

# Routes begin here

# Informational page with Copyright status and list of valid commands
# Available in xml only
get '/info.?:format?' do
  content_type 'application/xml'
  builder do |xml|
    xml.instruct!
    xml.s25(:error, :'xmlns:s25' => 'http://search25.ac.uk/ns/0.1', :cmd => request.path) do
      xml.s25(:message, 'The Search25 API provides library and access data for M25 member institutions. Available commands for this API are listed below. To the extent possible under UK law, the M25 Consortium of Academic Libraries has waived all copyright or neighbouring rights to data returned by the API')
      xml.s25(:license, :'http://creativecommons.org/publicdomain/zero/1.0/')
      xml.s25(:publisher, :'http://www.m25lib.ac.uk')
      xml.s25(:message, 'Further details of operation are available from http://docs.search25.ac.uk/api:operations')
      xml.tag!('s25:commands') do
        File.open($DATAPATH).each do |line| 
          (cmd, desc) = line.split(' - ')
          xml.s25(:command, desc, :cmd => cmd) 
        end
      end
    end
  end
end

# forms methods return form requirements for submission by an editor
# See http://restful-api-design.readthedocs.org/en/latest/forms.html for the idea behind this
# URLs without id return a blank form for creation of new entries
# URLs with id return an existing entry for editing
# forms methods must precede corresponding data methods or 'form' will
# be interpreted as an id if no format is specified

get '/schemes/form.?:format?' do
  
  empty_form('scheme', params[:format])

end

get '/schemes/:id/form.?:format?' do

  sql = "SELECT * FROM s25_scheme WHERE scheme_id='#{params[:id]}'"
  populated_form('scheme', sql, params[:format])

end

get '/institutions/form.?:format?' do
  
  empty_form('institution', params[:format])

end

get '/institutions/:m25_code/form.?:format?' do

  sql = "SELECT * FROM s25_institution WHERE active=true AND m25_code='#{params[:m25_code]}'"
  populated_form('institution', sql, params[:format])

end

get '/libraries/form.?:format?' do

  code = 
  empty_form('library', params[:format], { 'm25_code' => params[:m25_code]} )

end

get '/institutions/:m25_code/libraries/:id/form.?:format?' do

  sql = "SELECT * FROM s25_library WHERE m25_code='#{params[:m25_code]}' AND library_id='#{params[:id]}'"
  populated_form('library', sql, params[:format])

end

# Fetch form fields to create a new z3950 server
get '/z3950server/form.?:format?' do

  empty_form('z3950server', params[:format], { 'm25_code' => params[:m25_code]} )

end

# Fetch form details for a single z3950 server in an institution
# Must specify z39_name
get '/institutions/:m25_code/z3950server/form.?:format?' do

  sql = "SELECT * FROM s25_z3950server WHERE m25_code='#{params[:m25_code]}' AND z39_name='#{params[:z39_name]}'"
  populated_form('z3950server', sql, params[:format])

end

get '/agreements/form.?:format?' do

  empty_form('agreement', params[:format], { 'm25_code' => params[:m25_code]} )

end

get '/agreements/:id/form.?:format?' do

  sql = "SELECT * from s25_access_rule WHERE access_rule_id=#{params[:id]}"
  populated_form('agreement', sql, params[:format])

end

# The main (non-form) public actions 

# for use by the front-page map only
# list all libraries with their names, parent institutions, and locations
get '/library_locations.?:format?' do

    sql = "SELECT l.full_name AS library_name, l.latitude, l.longitude, s.full_name AS institution_name, s.m25_code FROM s25_library AS l LEFT JOIN s25_institution AS s ON s.m25_code = l.m25_code WHERE s.active=true AND l.active=true ORDER BY s.m25_code"
  res = $DBCONN.getConn.exec(sql)
  case params[:format]
    when 'json'
      {:library_locations => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'library_locations', 'library_locations', 'library_name' )
  end 

end

# List all member institutions
get '/institutions.?:format?' do
  
  sql = "SELECT i.m25_code, i.domain, i.copac_code, i.isil_code, i.sconul_code, i.full_name, i.short_name, i.sort_name, i.library_url, i.sconul_url, i.active, i.extra_info, (SELECT z.active FROM s25_z3950server AS z WHERE z.m25_code=i.m25_code AND z.source_type='library') AS has_target,  (SELECT z2.active FROM s25_z3950server AS z2 WHERE z2.m25_code=i.m25_code AND z2.source_type='uls') AS in_uls, (SELECT count(*) FROM s25_library AS l WHERE l.m25_code=i.m25_code AND l.active=true) as no_libraries  FROM s25_institution AS i WHERE i.m25_member=true "

  # allow (optional) disabling of inactive members
  if (params[:active] =~ /^true|false$/)
    sql = sql + " AND i.active=#{params[:active]} " 
  end

  sql = sql +  "ORDER BY i.sort_name";

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)

  case params[:format]
    when 'json'
      {:institutions => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'institution', 'institutions', 'm25_code' )
  end 

end

# Fetch a single institution
get '/institutions/:codeval' do

  # kludge for sinatra bug - can't handle named param + format
  (codeval, format) = params[:codeval].split('.')

  sql = "SELECT i.m25_code, i.domain, i.copac_code, i.isil_code, i.sconul_code, i.full_name, i.short_name, i.sort_name, i.library_url, i.sconul_url, i.extra_info, i.active FROM s25_institution AS i WHERE "

  sql += select_institutional_code(params[:code], codeval)

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case format
    when 'json'
      {:institutions => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'institution', 'institutions', 'm25_code' )
  end 

end

# update or create single institution
put '/institutions/:m25_code' do

    database_update( params, 's25_institution', 'institution', " WHERE m25_code='#{params[:m25_code]}'", "/institutions/#{params[:m25_code]}", ['GET', 'PUT'] )

end

# Delete a single institution
delete '/institutions/:m25_code' do

  database_delete( params, 's25_institution', 'institution', ['m25_code'] )

end

# fetch a single library
get '/institutions/:m25_code/libraries/:codeval' do

  # kludge for sinatra bug - can't handle named param + format
  (codeval, format) = params[:codeval].split('.')

  sql = "SELECT l.library_id, l.full_name, l.internal_name, l.home_url, l.opac_url, l.opening_hours_url, l.visitor_information_url, l.address, l.postcode, l.phone, l.email, l.latitude, l.longitude, l.easting, l.northing, l.district, l.district_uri, l.region, l.region_uri, l.find_tube, l.find_bus, l.find_train, l.find_other, l.find_parking, l.notes FROM s25_library AS l WHERE l.m25_code='#{params[:m25_code]}' AND l.library_id=#{codeval}"

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case format
    when 'json'
      {:libraries => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'library', 'libraries', 'library_id' )
  end 

end

# update single library
put '/institutions/:m25_code/libraries/:library_id' do

    database_update( params, 's25_library', 'library', " WHERE m25_code='#{params[:m25_code]}' AND library_id=#{params[:library_id]}", "/institutions/#{params[:m25_code]}/libraries/#{params[:library_id]}", ['GET', 'PUT'] )

end

# create single library
post '/institutions/:m25_code/libraries' do

    database_create( params, 's25_library', 'library_id', 'library', "/institutions/#{params[:m25_code]}/libraries/", ['GET', 'PUT'] )

end

# Delete a single library
delete '/institutions/:m25_code/libraries/:library_id' do

  database_delete( params, 's25_library', 'library', ['m25_code', 'library_id'] )

end

# Fetch the libraries in a single institution
get '/institutions/:codeval/libraries.?:format?' do

  # return the institutional data together with the library data
  # Includes a comma-separated list of Subject headings for each library
  sql = "SELECT i.m25_code, i.domain, i.copac_code, i.isil_code, i.sconul_code, i.full_name, i.short_name, i.sort_name, i.library_url, i.sconul_url, i.extra_info, i.active FROM s25_institution AS i WHERE "

  sql += select_institutional_code(params[:code], params[:codeval])

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)

  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  inst = res[0]

  m25_code = inst['m25_code']

  and_clauses = Array.new

  and_clauses.push(" l1.m25_code='#{m25_code}'")

  if params[:active] =~ /^true|false$/
    and_clauses.push(" l1.active=#{params[:active]}")
  elsif params[:active] == 'any'  # no active clause
  else
    and_clauses.push(" l1.active=true")  # default
  end

  sql = "SELECT l1.library_id, l1.full_name, l1.internal_name, l1.home_url, l1.opac_url, l1.opening_hours_url, l1.visitor_information_url, l1.address, l1.postcode, l1.phone, l1.email, l1.latitude, l1.longitude, l1.easting, l1.northing, l1.district, l1.district_uri, l1.region, l1.region_uri, l1.find_tube, l1.find_bus, l1.find_train, l1.find_other, l1.find_parking, l1.notes, (SELECT string_agg(s.subject, ', ') FROM s25_subject_authority AS s, s25_library_to_subject AS ls WHERE ls.m25_code='#{m25_code}' AND ls.library_id=l1.library_id AND s.subject_id=ls.subject_id) AS subjects FROM s25_library AS l1 WHERE " + and_clauses.join(' AND ') + ' ORDER BY l1.full_name' 

  #libs  = settings.pg.exec(sql) 
  libs = $DBCONN.getConn.exec(sql)

  case params[:format]
    when 'json'
      {:institution => inst}.to_json
      {:libraries => result_to_hasharray(libs)}.to_json
    else # default is xml
      builder do |xml|
        xml.instruct!
        xml.institution(:m25_code => inst['m25_code']) do
          inst.each do |key, val|
            xml.tag!(key, val)
          end
          xml.libraries do
            libs.each do |row|
              xml.library(:id => row['library_id']) do
                row.each do |key, val|
                  xml.tag!(key, val)
                end
              end
            end
          end
        end
      end
  end # end switch
end

# list all the libraries
get '/libraries.?:format?' do

  sql = "SELECT l.m25_code, l.library_id, l.full_name, i.full_name AS institution, l.internal_name, l.home_url, l.opac_url, l.opening_hours_url, l.visitor_information_url, l.address, l.postcode, l.phone, l.email, l.latitude, l.longitude, l.easting, l.northing, l.district, l.district_uri, l.region, l.region_uri, l.m25_code, l.find_tube, l.find_bus, l.find_train, l.find_other, l.find_parking, l.notes, l.active FROM s25_library AS l, s25_institution AS i WHERE i.m25_code= l.m25_code"

  if (params[:active] =~ /^false$/)
    sql = sql + " AND l.active=false "
  else
    sql = sql + " AND i.active=true AND l.active=true "
  end

  sql += " ORDER BY l.full_name"

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)

  case params[:format]
    when 'json'
      {:libraries => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'library', 'libraries', 'library_id')
  end # end switch

end

# list all Z39.50 servers
get '/z3950server.?:format?' do

  # only show zservers for active members
  sql ="SELECT z.z39_name, z.m25_code, z.source_type, z.linkback_url, z.active FROM s25_z3950server AS z, s25_institution as i WHERE z.m25_code=i.m25_code AND i.active=true"

  and_clauses = Array.new

  # but the zserver itself may be inactive
  if (params[:active] =~ /^true|false$/)
    and_clauses.push(" z.active=#{params[:active]} ") 
  end

  if (params[:source_type] =~ /^library|uls|archive$/)
    and_clauses.push(" z.source_type='#{params[:source_type]}' ") 
  end

  if and_clauses.length > 0 then
    sql += ' AND ' + and_clauses.join(' AND')
  end 

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case params[:format]
    when 'json'
      {:z3950servers => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'z3950server', 'z3950servers', 'z39_name')
  end # end switch

end

# list Z39.50 servers for a particular institution
# May specify z39_name to return a single server
get '/institutions/:code/z3950server.?:format?' do

  sql ="SELECT z.z39_name, z.m25_code, z.source_type, z.linkback_url, z.active FROM s25_z3950server AS z, s25_institution as i WHERE z.m25_code=i.m25_code AND i.m25_code='#{params[:code]}'"

  if (params[:active] =~ /^true|false$/)
    sql += " AND z.active=#{params[:active]} "
  elsif params[:active] == 'any'  # no active clause
  else
    sql += " AND z.active=true "  # default
  end

  if params[:z39_name]
    sql += " AND z.z39_name='#{params[:z39_name]}'"
  end

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case params[:format]
    when 'json'
      {:z3950servers => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'z3950server', 'z3950servers', 'z39_name')
  end # end switch

end


# create or modify a single z3950 server in an institution
# Must specify z39_name
put '/institutions/:m25_code/z3950server' do

    database_update( params, 's25_z3950server', 'z3950server', " WHERE m25_code='#{params[:m25_code]}' AND z39_name='#{params[:z39_name]}'", "/institutions/#{params[:m25_code]}/z3950?z39_name=#{params[:z39_name]}", ['GET', 'PUT'] )

end

# Delete a single z3950 server
# Must specify z39_name
delete '/institutions/:m25_code/z3950server' do

  database_delete( params, 's25_z3950server', 'z3950server', ['m25_code', 'z39_name'] )

end

# List the Access Schemes used by M25 members
get '/schemes.?:format?' do
  sql = 'SELECT scheme_id, name, sort_number, url FROM s25_scheme ORDER BY sort_number'
  #res  = settings.pg.exec(
  res = $DBCONN.getConn.exec(sql)
  
  case params[:format]
    when 'json'
      {:schemes => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'scheme', 'schemes', 'scheme_id')
  end # end switch

end

# Get details of a single Access Scheme
get '/schemes/:id' do

  # kludge for sinatra bug - can't handle named param + format
  (id, format) = params[:id].split('.')

  sql = "SELECT scheme_id AS id, name AS scheme, url FROM s25_scheme WHERE scheme_id='#{id}'"
  #res  = settings.pg.exec(
  res = $DBCONN.getConn.exec(sql)

  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case format
    when 'json'
      {:schemes => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'scheme', 'schemes', 'scheme_id')
  end # end switch

end

# Create or edit a single Access Scheme
put '/schemes/:scheme_id' do

  database_update( params, 's25_scheme', 'scheme', " WHERE scheme_id='#{params[:scheme_id]}'", "/schemes/#{params[:scheme_id]}", ['GET', 'PUT'] )

end

# Delete a single access scheme
delete '/schemes/:scheme_id' do

  database_delete( params, 's25_scheme', 'scheme', ['scheme_id'] )

end

# List all the institutions which are members of a particular scheme
get '/schemes/:id/institutions.?:format?' do
  
  sql = "SELECT i.m25_code, i.domain, i.copac_code, i.isil_code, i.full_name, i.short_name, i.sort_name, i.library_url, i.active FROM s25_institution AS i LEFT JOIN s25_institution_to_scheme AS s ON s.m25_code=i.m25_code WHERE s.scheme_id='#{params[:id]}'"

  if (params[:active] =~ /^true|false$/)
    sql = sql + " AND i.active=#{params[:active]} " 
  end

  sql = sql +  "ORDER BY i.sort_name";

  #res = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)

  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case params[:format]
    when 'json'
      {:institutions => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'institution', 'institutions', 'm25_code')
  end # end switch

end

# List all subjects in the Subject Authority Table
get '/subjects.?:format?' do

  sql = "SELECT s.subject_id, s.subject, s.ukat_id, s.concept_uri, s.ukat_url FROM s25_subject_authority AS s ORDER BY s.subject"
  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  
  case params[:format]
    when 'json'
      {:subjects => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'subject', 'subjects', 'subject_id')
  end # end switch

end

# Get details of a single subject
get '/subjects/:id' do

  # kludge for sinatra bug - can't handle named param + format
  (id, format) = params[:id].split('.')

  sql = "SELECT subject_id, subject, concept_uri, ukat_url, ukat_id FROM s25_subject_authority WHERE subject_id='#{id}'"
  #res  = settings.pg.exec(
  res = $DBCONN.getConn.exec(sql)

  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case format
    when 'json'
      {:schemes => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'subject', 'subjects', 'subject_id')
  end # end switch

end

# List all institutions which cover a particular subject
get '/subjects/:id/institutions.?:format?' do

  sql = "SELECT i.m25_code, i.domain, i.copac_code, i.isil_code, i.full_name, i.short_name, i.sort_name, i.library_url, i.active FROM s25_institution AS i LEFT JOIN s25_library_to_subject AS s ON s.m25_code=i.m25_code WHERE s.subject_id='#{params[:id]}'"

  if (params[:active] =~ /^true|false$/)
    sql = sql + " AND i.active=#{params[:active]} " 
  end

  sql = sql +  "ORDER BY i.sort_name";

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  
  case params[:format]
    when 'json'
      {:targets => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'institution', 'institutions', 'm25_code')
  end # end switch
end
  
# List all subjects covered by a single institution
get '/institutions/:codeval/subjects.?:format?' do

  sql = "SELECT s.subject_id, s.subject, s.ukat_id, s.concept_uri, s.ukat_url FROM s25_institution AS i LEFT JOIN s25_library AS l ON l.m25_code=i.m25_code LEFT JOIN s25_library_to_subject AS lts ON lts.library_id=l.library_id AND lts.m25_code=i.m25_code LEFT JOIN s25_subject_authority AS s ON lts.subject_id=s.subject_id WHERE ";

  sql += select_institutional_code(params[:code], params[:codeval])

  sql += ' ORDER BY s.subject';

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  inst = res[0]

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)

  case params[:format]
    when 'json'
      {:subjects => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'subject', 'subjects', 'subject_id' )
  end # end switch

end

# List all subjects covered by a single library
get '/institutions/:codeval/libraries/:id/subjects.?:format?' do

  sql = "SELECT s.subject_id, s.subject, s.ukat_id, s.concept_uri, s.ukat_url FROM s25_institution AS i LEFT JOIN s25_library_to_subject AS lts ON lts.library_id=#{params[:id]} AND lts.m25_code=i.m25_code LEFT JOIN s25_subject_authority AS s ON lts.subject_id=s.subject_id WHERE "

  sql += select_institutional_code(params[:code], params[:codeval])

  sql += " ORDER BY s.subject"

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  inst = res[0]

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)

  case params[:format]
    when 'json'
      {:subjects => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'subject', 'subjects', 'subject_id' )
  end # end switch

end

# List all roles
get '/roles.?:format?' do

  sql = "SELECT r.role_id, r.name FROM s25_role AS r ORDER BY r.role_id"
  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  
  case params[:format]
    when 'json'
      {:roles => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'role', 'roles', 'role_id')
  end # end switch

end

# list all entitlement agreements for an institution
get '/institutions/:m25_code/agreements.?:format?' do

  if (params[:m25_code] !~ /^[a-z]{2,5}$/) 
    raise InvalidParameterError, 'no institution selected'
  end 

  sql = "SELECT r.access_rule_id, r.m25_code, r.scheme_id, r.affiliation, r.role_id, r.notes AS requirements, r.charges FROM s25_access_rule AS r WHERE m25_code='" + params[:m25_code] + "'" 

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case params[:format]
    when 'json'
      {:access_rules => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'access_rule', 'access_rules', 'access_rule_id' )
  end # end switch
end

# create single access_rule
post '/institutions/:m25_code/agreements' do

  database_create( params, 's25_access_rule', 'access_rule_id', 'agreement', "/institutions/#{params[:m25_code]}/agreements/", ['GET', 'PUT'] )

end

# edit single access_rule
put '/institutions/:m25_code/agreements/:access_rule_id' do

  database_update( params, 's25_access_rule', 'agreement', " WHERE access_rule_id='#{params[:access_rule_id]}'", "/institutions/#{params[:m25_code]}/agreements/#{params[:access_rule_id]}", ['GET', 'PUT'] )

end

# Delete a single access rule
delete '/institutions/:m25_code/agreements/:access_rule_id' do

  database_delete( params, 's25_access_rule', 'agreement', ['access_rule_id'] )

end

# what can a user do at an institution's libraries?
get '/institutions/:codeval/entitlements.?:format?' do

  # make sure user has chosen a valid institution to visit first
  sql = "SELECT m25_code FROM s25_institution AS i WHERE "
  sql += select_institutional_code(params[:code], params[:codeval])
  
  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  m25_code = res[0]['m25_code']

  role = domain = ''
  if (params[:affiliation] =~ /^[\w]+\@[\w\.]+$/) # right format
    (role, domain) = params[:affiliation].split('@')
  else
    raise InvalidParameterError, 'no affiliation'
  end
  
  sql = "SELECT i.m25_code AS affiliation, r.role_id FROM s25_role AS r, s25_institution AS i WHERE i.domain='#{domain}' AND r.role_id='#{role}'"
  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples < 1 then
    raise InvalidParameterError, 'bad affiliation'
  end  

  affiliation = res[0]['affiliation']

  sql = "SELECT r.access_rule_id, r.m25_code, r.scheme_id, s.name AS scheme_name, s.url AS scheme_url, r.notes AS requirements, r.charges, BIT_OR(v.value) AS entitlement_value, (SELECT name FROM s25_entitlement_value WHERE value=MAX(v.value)) AS entitlement_name FROM s25_access_rule AS r LEFT JOIN s25_entitlement AS e ON r.access_rule_id=e.access_rule_id LEFT JOIN s25_entitlement_value AS v ON e.name=v.name LEFT JOIN s25_scheme AS s ON r.scheme_id=s.scheme_id GROUP BY r.access_rule_id, r.m25_code, r.scheme_id, s.name, s.url, r.notes, r.charges, r.role_id, r.affiliation HAVING r.role_id='#{role}' AND r.m25_code='#{m25_code}' AND ((r.affiliation='#{affiliation}') OR (r.scheme_id IN (SELECT scheme_id FROM s25_institution_to_scheme WHERE m25_code='#{affiliation}')))"

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  case params[:format]
    when 'json'
      {:access_rules => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'access_rule', 'access_rules', 'access_rule_id' )
  end # end switch

end

# at which institutions does a user have a particular entitlement?
get '/entitlements/:id/institutions.?:format?' do

  # make sure user has chosen a valid entitlement first
  sql = "SELECT name, value FROM s25_entitlement_value AS v WHERE name='#{params[:id]}'"
  
  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, 'no results'
  end  

  entitlement = res[0]['name']
  entitlement_value = res[0]['value']

  role = domain = ''
  if (params[:affiliation] =~ /^[\w]+\@[\w\.]+$/) # right format
    (role, domain) = params[:affiliation].split('@')
  else
    raise InvalidParameterError, 'no affiliation'
  end
  
  sql = "SELECT i.m25_code AS affiliation, r.role_id FROM s25_role AS r, s25_institution AS i WHERE i.domain='#{domain}' AND r.role_id='#{role}'"
  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples < 1 then
    raise InvalidParameterError, 'bad affiliation'
  end  

  affiliation = res[0]['affiliation']

  sql = "SELECT DISTINCT i.m25_code, i.domain, i.copac_code, i.isil_code, i.sconul_code, i.full_name, i.short_name, i.sort_name, i.library_url, i.sconul_url, i.active, r.scheme_id FROM s25_institution AS i LEFT JOIN s25_access_rule AS r ON r.m25_code=i.m25_code  LEFT JOIN s25_entitlement AS e ON e.access_rule_id=r.access_rule_id WHERE e.name='#{entitlement}' AND r.role_id='#{role}' AND i.active=true AND ((r.affiliation='#{affiliation}') OR (r.scheme_id IN (SELECT scheme_id FROM s25_institution_to_scheme WHERE m25_code='#{affiliation}'))) ORDER BY i.sort_name"

  #res  = settings.pg.exec(sql)
  res = $DBCONN.getConn.exec(sql)
  if res.ntuples == 0 then
    raise NoResultError, sql
  end  

  case params[:format]
    when 'json'
      {:institutions => result_to_hasharray(res)}.to_json
    else # default is xml 
      result_to_xml(res, 'institution', 'institutions', 'm25_code' )
  end # end switch

end

  # return server setting (test, dev, prod)
  # 
  get '/admin' do
    protected!
    ENV['RACK_ENV']
    settings.env
  end
__END__

