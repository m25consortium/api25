-- Conventions
-- Table names are singular, lower case, with underscores between words
-- Table names including '_to_' are joining tables only
-- Table names are prefixed with 's25' to allow loading into other dbs
-- for data munging purposes
-- Primary keys where practical are called [table_name]_id wherever they
-- are used (with the 's25_' dropped). The exceptions (m25_code, z39_name)
-- are exposed to the user by this name.
-- Foreign keys keep the name used in the table they reference
-- Field names are also lower case words separated by underscores

CREATE LANGUAGE plpgsql;

--
-- Name: institution; Type: TABLE; Schema: public;  
--

CREATE TABLE s25_institution (
    -- legible primary key exposed in urls
    m25_code CHARACTER VARYING(8) PRIMARY KEY,
    -- eg wmin.ac.uk, used to generate affiliations
	-- see also eduPersonScopedAffiliation
    domain CHARACTER VARYING(100) NOT NULL,
    -- COPAC identifier if it exists
    copac_code CHARACTER VARYING(10),
    -- BL (Marc) Isil identifier if it exists
    isil_code CHARACTER VARYING(10),
    -- Sconul identifier if it exists
    sconul_code INTEGER,
    -- Official name of institution
    full_name CHARACTER VARYING(255) NOT NULL,
    -- Shorter name of institution for abbreviated display
    short_name CHARACTER VARYING(30) NOT NULL,
    -- Name to use in alphabetical sorts
    sort_name CHARACTER VARYING(100) NOT NULL,
    -- Institutional library website page (not opac)
    library_url CHARACTER VARYING(255),
    -- Sconul contact details page
    sconul_url CHARACTER VARYING(255),
    -- hide institution from users unless true
    active BOOLEAN NOT NULL DEFAULT false,
    -- id used in old m25 system institution table (for data synching only)
    former_m25_id INTEGER,
    -- Any internal notes (not displayed)
    note TEXT,
    -- Any special notes to be displayed at head of library page
    extra_info CHARACTER VARYING(511),
    updated_by TEXT,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION update_updated_time()
    RETURNS TRIGGER AS '
        BEGIN
            NEW.updated_time = NOW();
            RETURN NEW;
        END;
'       LANGUAGE 'plpgsql';


CREATE TRIGGER update_modtime BEFORE UPDATE
    ON s25_institution FOR EACH ROW EXECUTE PROCEDURE
       update_updated_time();


ALTER TABLE public.s25_institution OWNER TO search25;

-- server_group types used to group z3950 servers. Default is 'library'
CREATE TYPE server_group AS ENUM ('library', 'uls', 'archive');

--
-- Name: z3950server; Type: TABLE; Schema: public
-- Z39.50 connection and attribute details are in the Pazpar2 configuration,
-- not in the database
--

CREATE TABLE s25_z3950server (
    -- Name in format aleph/soas, must match pazpar2 config file
    z39_name CHARACTER VARYING(50) PRIMARY KEY,
    -- institution primary key
    m25_code CHARACTER VARYING(8) REFERENCES s25_institution(m25_code),
    -- server group
    source_type server_group DEFAULT('library'),
    -- Opac source address for catalogue items
    -- Four underscores are marker for bibnum substitution slot
    linkback_url TEXT,
    -- hide server from users unless true
    active BOOLEAN NOT NULL DEFAULT false,
    -- Any internal notes (not displayed)
    note TEXT,
    updated_by TEXT,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER update_modtime BEFORE UPDATE
    ON s25_z3950server FOR EACH ROW EXECUTE PROCEDURE
       update_updated_time();

ALTER TABLE public.s25_z3950server OWNER TO search25;

--
-- Name: scheme; Type: TABLE; Schema: public;  
--

CREATE TABLE s25_scheme (
    scheme_id CHARACTER VARYING(20) PRIMARY KEY,
	-- printable name
    name CHARACTER VARYING(50) NOT NULL,
	-- order by quality of access
	sort_number integer NOT NULL,
	-- general information about this scheme
    url CHARACTER VARYING(255),
	-- internal notes
    note TEXT,
    updated_by TEXT,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER update_modtime BEFORE UPDATE
    ON s25_scheme FOR EACH ROW EXECUTE PROCEDURE
       update_updated_time();

ALTER TABLE public.s25_scheme OWNER TO search25;

--
-- Name: institution_to_scheme; Type: TABLE; Schema: public;  
--

CREATE TABLE s25_institution_to_scheme (
    m25_code CHARACTER VARYING(8) REFERENCES s25_institution(m25_code) ON DELETE CASCADE,
    scheme_id CHARACTER VARYING(20) REFERENCES s25_scheme(scheme_id) ON DELETE CASCADE,
    PRIMARY KEY(m25_code, scheme_id)
);

ALTER TABLE public.s25_institution_to_scheme OWNER TO search25;

--
-- Name: library; Type: TABLE; Schema: public;  
--

CREATE TABLE s25_library (
    library_id SERIAL NOT NULL,
    -- institution primary key
    m25_code CHARACTER VARYING(8) REFERENCES s25_institution(m25_code),
    -- displayed name
    full_name CHARACTER VARYING(100) NOT NULL,
    -- name used in Marc holding information
    internal_name CHARACTER VARYING(100),
    -- URL for library web page
    home_url CHARACTER VARYING(255),
    -- URL for opac (may be shared with other libraries)
    opac_url CHARACTER VARYING(255),
    -- URL for opening hours
    opening_hours_url TEXT,
    -- URL for visitor page
    visitor_information_url TEXT,
    -- Physical address
    address TEXT,
    postcode TEXT,
    phone TEXT,
    email TEXT,
    -- latitude and longitude derived from postcode
    -- using http://www.uk-postcodes.com/postcode/
    latitude REAL,
    longitude REAL,
    -- easting, northing also from uk-postcodes.com
    -- use for distance calculations (lat/long for map pins)
    easting INTEGER,
    northing INTEGER,
    -- district is administrative district from uk-postcodes.com
    district CHARACTER VARYING(50),
    district_uri CHARACTER VARYING(100),
    -- region is 'within' for constituency in ordnancesurvey doc
    region CHARACTER VARYING(50),
    region_uri CHARACTER VARYING(100),
    -- Access TEXT
    find_tube TEXT,
    find_bus TEXT,
    find_train TEXT,
    find_other TEXT,
    find_parking TEXT,
    notes TEXT,
    -- Any internal notes (not displayed)
    note TEXT,
    updated_by TEXT,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(library_id, m25_code)
);

CREATE TRIGGER update_modtime BEFORE UPDATE
    ON s25_library FOR EACH ROW EXECUTE PROCEDURE
       update_updated_time();

ALTER TABLE public.s25_library OWNER TO search25;

--
-- Name: subject; Type: TABLE; Schema: public; 
--

CREATE TABLE s25_subject_authority (
    -- unique identifier
    subject_id SERIAL PRIMARY KEY,
	-- printable description
    subject TEXT,
	-- Numeric ID from ukat
    ukat_id INTEGER,
	-- SKOS definitional URI
    concept_uri CHARACTER VARYING(100),
        -- UKAT home page for term
    ukat_url CHARACTER VARYING(100),
    -- Any internal notes (not displayed)
    note TEXT,
    updated_by TEXT,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER update_modtime BEFORE UPDATE
    ON s25_subject_authority FOR EACH ROW EXECUTE PROCEDURE
       update_updated_time();

ALTER TABLE public.s25_subject_authority OWNER TO search25;


--
-- Name: library_to_subjects; Type: TABLE; Schema: public;  
--

CREATE TABLE s25_library_to_subject (
    m25_code CHARACTER VARYING(8),
    library_id INTEGER,
    subject_id INTEGER REFERENCES s25_subject_authority(subject_id) ON DELETE CASCADE,
    note TEXT,
	FOREIGN KEY(m25_code, library_id) REFERENCES s25_library(m25_code, library_id),
    PRIMARY KEY(m25_code, library_id, subject_id)
);

ALTER TABLE public.s25_library_to_subject OWNER TO search25;

--
-- Name: role; Type: TABLE; Schema: public;  
-- see also eduPersonScopedAffiliation (ie. role + domain)
--

CREATE TABLE s25_role (
	-- the id is a text abbreviation for the role
    role_id character varying(8) PRIMARY KEY,
	-- name is the printable version
    name character varying(50)
);

ALTER TABLE public.s25_role OWNER TO search25;

--
-- Name: accessrule; Type: TABLE; Schema: public;  
--

CREATE TABLE s25_access_rule (
    -- unique identifier
	access_rule_id SERIAL PRIMARY KEY,
	-- role of user accessing institution
	role_id CHARACTER VARYING(8) REFERENCES s25_role(role_id),
	-- institution being accessed 
	m25_code CHARACTER VARYING(8) REFERENCES s25_institution(m25_code),
	-- normally, EITHER affiliation OR scheme_id will be populated
	-- institution of user accessing institution if bilateral rule
	affiliation CHARACTER VARYING(63) REFERENCES s25_institution(m25_code),
	-- scheme involved in this rule if scheme based rule
	scheme_id CHARACTER VARYING(20) REFERENCES s25_scheme ON DELETE CASCADE,
	-- printable textual description of this access rule
	notes text,
	-- Printable explanation of charges, if any
	charges text,
	-- Internal note
	note text, 
    updated_by TEXT,
    updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER update_modtime BEFORE UPDATE
    ON s25_access_rule FOR EACH ROW EXECUTE PROCEDURE
       update_updated_time();

ALTER TABLE public.s25_access_rule OWNER TO search25;

CREATE TABLE s25_entitlement (
	access_rule_id INTEGER REFERENCES s25_access_rule(access_rule_id) ON DELETE CASCADE,
	name CHARACTER VARYING(20) NOT NULL
);

ALTER TABLE public.s25_entitlement OWNER TO search25;

CREATE TABLE s25_entitlement_value (
	name CHARACTER VARYING(20) PRIMARY KEY,
	value INTEGER NOT NULL
);
